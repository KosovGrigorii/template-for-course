from django.db import models


class User(models.Model):
    telegram_id = models.PositiveIntegerField(
        primary_key=True,
        verbose_name='user ID'
    )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    username = models.CharField(max_length=255, null=True, unique=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True, unique=True)

    def __str__(self):
        return f'{self.first_name}'

    class Meta:
        verbose_name = 'Person'
        verbose_name_plural = 'People'