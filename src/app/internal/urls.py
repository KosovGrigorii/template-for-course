from django.urls import path
from app.internal.transport.rest.handlers import UserDetailView

urlpatterns = [
    path('me/<int:telegram_id>', UserDetailView.as_view())
]
