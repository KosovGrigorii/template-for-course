from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.models.user import User
from app.internal.models.admin_user import AdminUser

@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    pass


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('telegram_id', 'username', 'first_name', 'last_name', 'phone_number')
