from app.internal.models.user import User
from telegram import Contact


# def check_phone_presence(*args):
#     def _check_phone_presence(f):
#         async def a_wrapper_accepting_arguments(update, _):
#             chat_id = update.message.chat_id
#             obj = await User.objects.aget(telegram_id=chat_id)
#             for field in args:
#                 value = getattr(obj, field)
#                 if value is None or value == '':
#                     await update.message.reply_text(f'You need to add your {field.name.replace("_", " ")} first')
#                     return
#             return await f(update, _)
#         return a_wrapper_accepting_arguments
#     return _check_phone_presence


# class UserService:
#     async def _extract_user(self, telegram_id: int) -> User:
#         return await User.objects.aget(telegram_id=telegram_id)
#
#     def _form_final_text(self, user: User, telegram: bool) -> str:
#         reply_text = '<p><b>That is all we know about you</b></p>'
#         for field in User._meta.fields:
#             value = getattr(user, field.name)
#             reply_text += f'<p><b>Your {field.name.replace("_", " ")}:</b> {value}</p>'
#         if telegram:
#             reply_text = reply_text.replace('<p>', '')
#             reply_text = reply_text.replace('</p>', '\n')
#         return reply_text
#
#     async def execute(self, telegram_id: int, telegram: bool = False) -> str:
#         user = await self._extract_user(telegram_id)
#         return self._form_final_text(user, telegram)


class UserService:
    async def save_user(self, telegram_id: int, first_name: str, last_name: str, username: str):
        p, _ = await User.objects.aupdate_or_create(
            telegram_id=telegram_id,
            defaults={
                'first_name': first_name,
                'last_name': last_name,
                'username': username,
            }
        )

    async def get_user(self, telegram_id: int) -> User:
        try:
            return await User.objects.aget(telegram_id=telegram_id)
        except User.DoesNotExist:
            return None

    async def set_phone(self, telegram_id: int, phone: Contact):
        await User.objects \
            .filter(telegram_id=telegram_id) \
            .aupdate(phone_number=phone)

    async def get_full_info(self, telegram_id: int, telegram: bool = False):
        user = await self.get_user(telegram_id)
        reply_text = ''
        if user is None:
            reply_text += '<p><b>Who are you?</b></p>'
        elif user.phone_number is None or user.phone_number == '':
            reply_text += '<p><b>You need to add your phone number!</b></p>'
        else:
            reply_text = '<p><b>That is all we know about you</b></p>'
            for field in User._meta.fields:
                value = getattr(user, field.name)
                reply_text += f'<p><b>Your {field.name.replace("_", " ")}:</b> {value}</p>'
        if telegram:
            reply_text = reply_text.replace('<p>', '')
            reply_text = reply_text.replace('</p>', '\n')
        return reply_text
