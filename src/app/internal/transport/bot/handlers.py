from app.internal.services.user_service import UserService, UserService
from telegram import Update, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import ConversationHandler, ContextTypes
from app.internal.models.user import User

user_service = UserService()


async def error_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(text=str(context.error))


async def save_info(update: Update, _) -> None:

    user = update.effective_user
    await user_service.save_user(user.id, user.first_name, user.last_name, user.username)
    reply_text = f'You can add your phone number using /set_phone command'
    await update.message.reply_text(
        text=reply_text,
    )


PHONE = range(1)


async def start(update: Update, _):
    con_keyboard = KeyboardButton(text="send phone number", request_contact=True)
    custom_keyboard = [[con_keyboard]]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    await update.message.reply_text(
        'Push the button to send your phone',
        reply_markup=reply_markup)

    return PHONE


async def phone(update: Update, _) -> int:
    telegram_id = update.message.chat_id
    phone_number = update.effective_message.contact.phone_number
    await user_service.set_phone(telegram_id, phone_number)
    await update.message.reply_text('Your phone number was saved\nYou now have access to /me command',
                                    reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


async def cancel(update: Update, _) -> int:
    await update.message.reply_text(
        'Phone number was not saved'
    )
    return ConversationHandler.END


# @check_phone_presence('phone_number')
async def get_info(update: Update, _) -> None:
    reply_text = await user_service.get_full_info(update.message.chat_id, True)
    await update.message.reply_text(
        text=reply_text,
        parse_mode='html'
    )
