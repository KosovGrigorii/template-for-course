from django.views import View
from django.http import HttpResponse
from app.internal.services.user_service import UserService


class UserDetailView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.service = UserService()

    async def get(self, request, telegram_id):
        reply_text = await self.service.get_full_info(telegram_id)
        return HttpResponse(reply_text)


