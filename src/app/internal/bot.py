from django.conf import settings
from telegram.ext import Application, CommandHandler, ConversationHandler, MessageHandler, filters
from app.internal.transport.bot.handlers import save_info, start, phone, PHONE, cancel, get_info, error_handler


def run_bot() -> None:
    app = Application.builder().token(settings.TOKEN).build()
    app.add_handler(CommandHandler('start', save_info))
    app.add_handler(CommandHandler('me', get_info))
    app.add_handler(ConversationHandler(
        entry_points=[CommandHandler('set_phone', start)],
        states={
            PHONE: [MessageHandler(filters.CONTACT, phone)]
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    ))
    app.add_error_handler(error_handler)
    app.run_polling()
